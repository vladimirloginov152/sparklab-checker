from pyspark.sql import SparkSession
from pyspark.sql.functions import pandas_udf
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType, ArrayType, StringType, StructType, StructField
from collections import Counter
import pandas as pd
import re
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def task1(df):
	df_posts_task1 = df.select("id",
                                 F.col("likes.count").alias("likes_count"),
                                 F.col("comments.count").alias("comments_count"),
                                 F.col("reposts.count").alias("reposts_count"))

	task1a_gold_result = df_posts_task1.sort(F.col("likes_count").desc())
	# 1 b
	task1b_gold_result = df_posts_task1.sort(F.col("comments_count").desc())
	# 1 c
	task1c_gold_result = df_posts_task1.sort(F.col("reposts_count").desc())
	return task1a_gold_result, task1b_gold_result, task1c_gold_result

def task2(df, df2):
	task2a_gold_result = df.groupBy('likerId').count().sort(F.col("count").desc())
	df_posts_task2b = df2.select(
    	F.col("owner_id").alias("follower_vk_id"),
    	F.col("copy_history.owner_id").alias("pub_id")
	)

	df_posts_task2b = df_posts_task2b.where(F.col("pub_id").isNotNull())
	task2b_gold_result = df_posts_task2b.groupBy('follower_vk_id').count().sort(F.col("count").desc())
	return task2a_gold_result, task2b_gold_result

def task3(df):
	df_posts_task3 = df.select(
    	F.col("owner_id").alias("user_id"),
    	F.col("id").alias("user_post_id"),
    	F.col("copy_history.owner_id").alias("pub_id"),
    	F.col("copy_history.id").alias("group_post_id")
	)

	df_posts_task3 = df_posts_task3.where(F.col("pub_id").getItem(0) == -94)

	df_posts_task3 = df_posts_task3.select("user_id",
	                                       "user_post_id",
	                                       "pub_id",
	                                       F.col("group_post_id").getItem(0).alias("group_post_id"))

	task3_gold_result = df_posts_task3.groupBy("group_post_id").agg(F.collect_list("user_post_id").alias("user_post_ids"))
	return task3_gold_result

def task5(df):
	df_followers_pub_posts_likes_task_5 = (df
	                                       .groupBy('likerId')
	                                       .agg(F.collect_list('ownerId').alias('users_ids')))
	contents = ArrayType(StructType([StructField("user", IntegerType(), False), StructField("likes_count", IntegerType(), False)]))
	counter_udf = F.udf(lambda x: Counter(x).most_common(), contents)
	task5_gold_result = df_followers_pub_posts_likes_task_5.select("likerId", counter_udf('users_ids').alias('top_users_liked_by'))
	return task5_gold_result

def task6(df):
	top_liked_users_by_task_6 = df

	top_liked_users_by_task_6 = top_liked_users_by_task_6.select(F.col("likerId"),
	                                                             (F.col("top_users_liked_by")
	                                                              .getItem(0)
	                                                              .alias("like_enjoyer_id")))
	# remove self - likers
	top_liked_users_by_task_6 = top_liked_users_by_task_6.filter('likerId != like_enjoyer_id.user')


	left_top_liked_users_by_task_6 = top_liked_users_by_task_6.select(F.col("likerId").alias("likerId_left"), F.col("like_enjoyer_id").alias("like_enjoyer_id_left"))

	probable_friends = left_top_liked_users_by_task_6.join(top_liked_users_by_task_6,
	                  (F.col("likerId_left") == F.col('like_enjoyer_id.user'))
	                  & (F.col('like_enjoyer_id_left.user') == F.col("likerId")),
	                                               'inner')

	task6_gold_result = probable_friends.select(F.col("likerId").alias("liker_id"), F.col("like_enjoyer_id"))

	return task6_gold_result
