import re
import sys
import importlib.util as imprtutil
from pyspark.sql import SparkSession
from pyspark.sql.functions import pandas_udf
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType, ArrayType, StringType, StructType, StructField
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import socket

try:
    spec = imprtutil.spec_from_file_location('student', sys.argv[1])
    student = imprtutil.module_from_spec(spec)
    sys.modules['student'] = student
    spec.loader.exec_module(student)
except:
    print('Import error')


def undesirable_libs_imported(script):
    """ Checks if the script imports undesirable libraries. """

    undesirable_libs = set(['os', 'sys', 'subprocess', 'multiprocessing', 'command'])
    modules_imported = set()
    with open(script, 'r') as opened_file:
        file_lines = opened_file.readlines()

        for line in file_lines:

            # import XXX
            match = re.match(r'import\s([\w\.]+)', line)
            if match:
                modules_imported.add(match.groups()[0])

            # from XXX
            match = re.match(r'from\s([\w\.]+)', line)
            if match:
                modules_imported.add(match.groups()[0])

    return len(undesirable_libs.intersection(modules_imported)) > 0


def line_without_comment(line):
    """ Cuts the comment part from a line. """

    if '#' in line:
        return line.split('#')[0]
    if '"""' in line:
        return line.split('"""')[0]
    if "'''" in line:
        return line.split("'''")[0]
    return line


def undesirable_operations_used(script):
    """ Checks if the script uses undesirable operations. """

    undesirable_operations = set(['spark', 'SparkSession', 'read', 'write', 'open'])
    with open(script, 'r') as opened_file:
        file_lines = opened_file.readlines()

        for line in file_lines:
            if any(operation in line_without_comment(line) for operation in undesirable_operations):
                return True

    return False

# Check of script content

script = sys.argv[1]


try:
    assert undesirable_libs_imported(script) is False
except AssertionError:
    print('Error: Undesirable libraries are imported')

try:
    assert undesirable_operations_used(script) is False
except AssertionError:
    print('Error: Undesirable operations are used')


# SparkSession initialization

LOCAL_IP = socket.gethostbyname(socket.gethostname())
spark = (
    SparkSession
        .builder
        # .master('k8s://https://10.32.7.103:6443')
        .config("spark.driver.host", LOCAL_IP)
        .config("spark.driver.bindAddress", "0.0.0.0")
        .config('spark.executor.instances', '2')
        .config('spark.executor.cores', '2')
        .config('spark.cores.max', '4')
        .config('spark.executor.memory', '4g')
        .config('spark.sql.execution.arrow.enabled', 'true')
        # .config('spark.kubernetes.namespace', 'tutor')
        # .config('spark.kubernetes.container.image', 'node03.st:5000/spark-executor:tutor')
        # .config("spark.kubernetes.container.image.pullPolicy", "Always")
        # .config('spark.kubernetes.executor.deleteOnTermination', 'false')
        .getOrCreate()
)

# import student script

try:
    spec = imprtutil.spec_from_file_location('student', sys.argv[1])
    student = imprtutil.module_from_spec(spec)
    sys.modules['student'] = student
    spec.loader.exec_module(student)
except:
    print('Import error')

# initial data for gold results calculation

posts_api_json = 'Data/posts_api.json'
# posts_likes_parquet = 'hdfs:///shared/bigdata20/posts_likes.parquet'
# followers_parquet = 'hdfs:///shared/bigdata20/followers.parquet'
# followers_info_json = 'hdfs:///shared/bigdata20/followers_info.json'
# followers_posts_api_final_json = 'hdfs:///shared/bigdata20/followers_posts_api_final.json'
followers_posts_likes_parquet = 'Data/followers_posts_likes.parquet'

df_posts = spark.read.json(posts_api_json)
# df_posts_likes = spark.read.parquet(posts_likes_parquet)
# df_followers = spark.read.parquet(followers_parquet)
# df_followers_info = spark.read.json(followers_info_json)
# df_followers_pub_posts = spark.read.json(followers_posts_api_final_json)
df_followers_pub_posts_likes = spark.read.parquet(followers_posts_likes_parquet)

task1 = student.task1(df_posts)
# task2 = student.task2(df_posts_likes, df_followers_pub_posts)
# task3 = student.task3(df_followers_pub_posts)
task5 = student.task5(df_followers_pub_posts_likes)
task6 = student.task6(task5)


# methods for tasks checking

def check_task1(df, N=100):
    task1a_student, task1b_student, task1c_student = df
    print(task1a_student, task1b_student, task1c_student)

    # 1
    df_posts_task1 = df_posts.select("id",
                                     F.col("likes.count").alias("likes_count"),
                                     F.col("comments.count").alias("comments_count"),
                                     F.col("reposts.count").alias("reposts_count"))

    # 1 a
    task1a_gold_result = df_posts_task1.sort(F.col("likes_count").desc())

    try:
        assert task1a_student.take(N) == task1a_gold_result.take(N)
        print('TASK 1a CORRECT')
    except:
        print('TASK 1a INCORRECT')
        difference = task1a_gold_result.subtract(task1a_student)
        if difference.rdd.isEmpty():
            difference = task1a_student.subtract(task1a_gold_result)

        difference.show(20)

    # 1 b
    task1b_gold_result = df_posts_task1.sort(F.col("comments_count").desc())

    try:
        assert task1b_student.take(N) == task1b_gold_result.take(N)
        print('TASK 1b CORRECT')
    except:
        print('TASK 1b INCORRECT')

    # 1 c
    task1c_gold_result = df_posts_task1.sort(F.col("reposts_count").desc())

    try:
        assert task1c_student.take(N) == task1c_gold_result.take(N)
        print('TASK 1c CORRECT')
    except:
        print('TASK 1c INCORRECT')


def check_task6(df, N=100):
    df_followers_pub_posts_likes = spark.read.parquet(followers_posts_likes_parquet)

    # 5
    df_followers_pub_posts_likes_task_5 = (df_followers_pub_posts_likes
                                           .groupBy('likerId')
                                           .agg(F.collect_list('ownerId').alias('users_ids')))
    contents = ArrayType(
        StructType([StructField("user", IntegerType(), False), StructField("likes_count", IntegerType(), False)]))
    counter_udf = F.udf(lambda x: Counter(x).most_common(), contents)
    task5_gold_result = df_followers_pub_posts_likes_task_5.select("likerId",
                                                                   counter_udf('users_ids').alias('top_users_liked_by'))

    # 6
    top_liked_users_by_task_6 = task5_gold_result

    top_liked_users_by_task_6 = top_liked_users_by_task_6.select(F.col("likerId"),
                                                                 (F.col("top_users_liked_by")
                                                                  .getItem(0)
                                                                  .alias("like_enjoyer_id")))
    # remove self - likers
    top_liked_users_by_task_6 = top_liked_users_by_task_6.filter('likerId != like_enjoyer_id.user')

    left_top_liked_users_by_task_6 = top_liked_users_by_task_6.select(F.col("likerId").alias("likerId_left"),
                                                                      F.col("like_enjoyer_id").alias(
                                                                          "like_enjoyer_id_left"))

    probable_friends = left_top_liked_users_by_task_6.join(top_liked_users_by_task_6,
                                                           (F.col("likerId_left") == F.col('like_enjoyer_id.user'))
                                                           & (F.col('like_enjoyer_id_left.user') == F.col("likerId")),
                                                           'inner')

    task6_gold_result = probable_friends.select(F.col("likerId").alias("liker_id"), F.col("like_enjoyer_id"))
    task6_gold_result = task6_gold_result.sort(F.col('likerId'), ascending=True)
    df = df.sort(F.col('likerId'), ascending=True)

    try:
        assert df.take(N) == task6_gold_result.take(N)
        print('TASK 6 CORRECT')
    except:
        print('TASK 6 INCORRECT')

# check_task1(task1, N=70)
# check_task6(task6)

